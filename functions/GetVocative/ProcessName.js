const Dict = require("./Dict");

module.exports = function ProcessName(raw) {
    const dict = Dict();
    const vocative = raw.split(' ')
        .map(n => (n.charAt(0).toUpperCase() + n.slice(1).toLowerCase()))
        .map(n => {
            let i = dict.findIndex(e => e[0] == n);
            return i == -1 ? undefined : dict[i][1];
        })
        .filter(n => n !== undefined)
        .join(' ');
        return vocative;
}
