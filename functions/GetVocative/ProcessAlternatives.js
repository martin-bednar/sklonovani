const ProcessName = require("./ProcessName");

module.exports = function ProcessAlternatives(primary, secondary, def) {
    let res = ProcessName(primary)
    if(res !== "" || (secondary === undefined && def === undefined))
        return res
    if(secondary !== undefined) {
    res = ProcessName(secondary)
    if(res !== "" || def === undefined)
        return res
    }
    return def
}
