const ProcessAlternatives = require("./ProcessAlternatives");

module.exports.handler = async function (event, context) {
  if (event.httpMethod == 'GET') {
    try {
      console.log(`Name: ${event.queryStringParameters.name}, headers: ${JSON.stringify(event.headers)}`);
      return {
          statusCode: 200,
          headers: {
            'content-type': 'text/plain;charset=UTF-8'
          },
          body: ProcessAlternatives(event.queryStringParameters.name, event.queryStringParameters.alternative, event.queryStringParameters["default"] )
        }
    } catch (err) {
      console.error(err); 
      console.error("Function failed for input: " + event.queryStringParameters.name); 
      return {
        statusCode: 500,
        headers: {
          'content-type': 'text/plain;charset=UTF-8'
        },
        body: ''
      }
    }
  }
  else {
    return {
          statusCode: 405, //method not allowed
          headers: {
            'content-type': 'text/plain;charset=UTF-8'
          },
          body: ''
        }
  }
}
