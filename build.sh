OUT="./public/names2.json"

echo '[' > "$OUT"

cat ./src/krestni_*   | 
    grep -v -e '^"1"'   |
    grep -v -e '^"2"'   |
    cut -d, -f2-        |
    sed 's/^/[/'        |
    sed 's/$/],/'       >> "$OUT"

echo ']' >> "$OUT"